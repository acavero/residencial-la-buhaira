<?php 
wp_reset_postdata();
if(comments_open() || have_comments()) : ?>
<div class="comments-area <?php print comments_open() || have_comments() ? '' : 'no-comments';?>">
	<div class="container">
		<?php if ( post_password_required() ) : ?>
					<p><?php esc_html_e( 'This post is password protected. Enter the password to view any comments ', 'narcos' ); ?></p>
				</div>

			<?php
			/* Stop the rest of comments.php from being processed,
			 * but don't kill the script entirely -- we still have
			 * to fully load the template.
			 */
			return;
			endif;?>

		<?php if ( have_comments() ) : ?>
			<h4 class="comment-title"><?php comments_number( esc_html__('No Comments','narcos'), esc_html__('1 Comentario','narcos'), '% '.esc_html__('Comentarios','narcos') ); ?></h4>
			<div class="comments_navigation page-numbers">
				<?php paginate_comments_links(array(
				'show_all'     => False,
				'end_size'     => 1,
				'mid_size'     => 2,
				'prev_next'    => True,
				'prev_text'    => '&larr;',
				'next_text'    => '&rarr;',
				'type'         => 'list',
				'add_args'     => False,
				'add_fragment' => ''
			)); ?>
			</div>

			<ul class="comments-list  clean-list">
				<?php wp_list_comments( array( 'callback' => 'narcos_custom_comments' , 'avatar_size'=>'115','style'=>'ul') ); ?>
			</ul>	
		
		<?php endif; ?>

		<?php 
		$args = array(
			'fields' => apply_filters( 'comment_form_default_fields', array(
				'author' => '<div class="row"><div class="col-sm-8"><input name="author" class="form-input check-value" type="text" value="' . esc_attr( $commenter[ 'comment_author' ] ) . '" aria-required="true" placeholder="'.esc_html__('Nombre','narcos').'"></div>',
				'email' => '<div class="col-sm-8"><input name="email" type="text" class="form-input check-value" value="' . esc_attr( $commenter[ 'comment_author_email' ] ) . '" aria-required="true" placeholder="'.esc_html__('Email','narcos').'"></div>',
				'url' => '<div class="col-sm-8"><input name="url" type="text" class="form-input check-value" placeholder="'.esc_html__('Web','narcos').'" value="' . esc_attr( $commenter[ 'comment_author_url' ] ) . '"></div></div>'
					)
			),

			'comment_notes_after' => '',
			'comment_notes_before' => '',
			'title_reply' => '',
			'comment_field' => '<textarea name="comment" class="form-input check-value" placeholder="'.esc_html__('Mensaje', 'narcos').'"></textarea>',
			'class_submit' => 'submit',
			'label_submit' => esc_html_x('Comentar','comment-form','narcos')
			);

	    	comment_form( $args );
		?>
		
	</div>
</div>
<?php endif; ?>