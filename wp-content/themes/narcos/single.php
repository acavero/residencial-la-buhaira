<?php get_header();
/**
 * Single post page
 */

$page_id = narcos_get_page_id();
$blog_page_id = get_option( 'page_for_posts' );
$sidebar_option = get_post_meta( $page_id, THEME_NAME . '_sidebar_position', true );

switch ($sidebar_option) {
	case 'as_blog':
		$s_id = $blog_page_id;	
		break;
	case 'full_width':
		$s_id = $page_id;
		break;
	case 'right':
		$s_id = $page_id;
		break;
	case 'left':
		$s_id = $page_id;
}

if(!empty($s_id))
	$sidebar = get_post_meta( $s_id, THEME_NAME . '_sidebar_position', true );
	$sidebar = empty($sidebar) ? 'right' : $sidebar;
?>

<?php get_template_part('templates/intro','box'); ?>

<?php while ( have_posts() ) : the_post(); $author_id = get_the_author_meta( 'ID' ); ?>
	<!-- Single Post Page Heading -->
	<div class="single-post-page-heading">
		<ul class="clean-list post-meta">
			<li><a href="<?php echo get_author_posts_url( $author_id ); ?>"><?php the_author(); ?></a></li>
			<li><?php the_time(get_option('date_format'));?></li>
			<li><?php the_category();?></li>
		</ul>
	</div>

	<!-- Blog Section -->
	<section class="section section-blog no-margin">
		<div class="row">
			<?php if($sidebar == "left"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>

			<div class="<?php print $sidebar == "full_width" ? 'col-lg-12 col-lg-offset-6 col-md-18 col-md-offset-3' : 'col-lg-12 col-lg-offset-2 col-md-15';?>">
				<div class="single-post-content">
					<?php the_content();?>
					<div class="post-navigation">
			            <?php wp_link_pages(array(
			                'next_or_number'   => 'number',
			                'nextpagelink'     => esc_html__( 'Next page','narcos' ),
			                'previouspagelink' => esc_html__( 'Previous page','narcos' ),
			                'pagelink'         => '%',
			                'echo' => 1
			            )); ?>
			        </div>
				</div>
			</div>

			<?php if($sidebar == "right"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>
		</div>
	</section>

	<!-- Blog Post Footer -->
	<div class="blog-post-footer">
		<div class="row">
			<div class="col-lg-12 col-lg-offset-2 col-md-15 col-sm-12">
				<?php narcos_share(); ?>
			</div>

			<div class="col-lg-8 col-lg-offset-2 col-md-9 col-sm-12">
				<div class="post_tagcloud">
					<?php the_tags();?>
				</div>
			</div>
		</div>
	</div>

	<?php comments_template();?>

<?php narcos_set_post_views(get_the_ID()); endwhile; ?>
<?php get_footer();?>