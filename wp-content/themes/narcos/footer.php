			<?php 
				$post_id = tt_get_page_id();
				$check = get_post_meta( $post_id, THEME_NAME . '_blog_shortcode', true );

				if(is_single('post')) {
					$page_id = get_option( 'page_for_posts' );
				} elseif (tesla_has_woocommerce() && is_product()) {
					$page_id = get_option( 'woocommerce_shop_page_id' );
				} else {
					$page_id = narcos_get_page_id();
				}

				$shortcode = $check ? get_post_meta( $post_id, THEME_NAME . '_page_shortcode', true ) : get_post_meta( $page_id, THEME_NAME . '_page_shortcode', true );
				echo do_shortcode($shortcode);
			?>
			</div>

			<!-- Footer -->
			<footer>
				<?php $social_platforms = array('facebook','twitter','google','pinterest','instagram','linkedin','dribbble','behance','youtube','flickr','rss');
                    $show_platform = false;
                    foreach($social_platforms as $platform) {
                        if (_go('show_' . $platform) || $show_platform)
                            $show_platform = true;
                        else
                            $show_platform = false;
                    }
                if($show_platform): ?>
				<div class="main-social-block">
					<!-- Rounded Border Bottom -->
					<div class="rounded-border-bottom">
						<span></span>
					</div>

					<div class="lazy-load">
						<div class="row">
							<div class="col-xs-24">
								<div class="social-block-group">
									<?php
										foreach($social_platforms as $platform)
			                                if (_go('social_platforms_'. $platform) && _go('show_' . $platform)) 
			                            		printf('<a href="%s" target="_blank"><span>%s</span><i class="icon-%s"></i></a>', _go('social_platforms_'. $platform), $platform, $platform)
										;?>
								</div>
							</div>
						</div>
					</div>

					<!-- Rounded Border Bottom -->
					<div class="rounded-border-bottom reversed">
						<span></span>
					</div>

					<!-- Block Icon -->
					<i class="icon-share2 block-icon"></i>
				</div>
				<?php endif;?>

				<!-- Footer Wrapper -->
				<div class="main-footer-wrapper">
					<?php get_sidebar('footer');?>

					<div class="container copyrights align-center">
<p><?php _eo('footer_info');?> </p>
						
					</div>
				</div>
			</footer>
		</div>
	</div>

	<?php wp_footer(); ?>
</body>
</html>