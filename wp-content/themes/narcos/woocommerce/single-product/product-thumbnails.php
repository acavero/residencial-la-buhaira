<?php
/**
 * Single Product Thumbnails
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product, $woocommerce;

$product_images_id = array();
$product_images_id[] = get_post_thumbnail_id();

$attachment_ids = array_merge( $product_images_id, $product->get_gallery_attachment_ids());

if ( count($attachment_ids) > 1 ) {
	?>
	<!-- Navigation -->
	<div class="tt-carousel nav-carousel" data-items-desktop="4" data-items-small-desktop="3" data-items-tablet="2" data-items-phone="2" data-infinite="true" data-dots="true" data-arrows="false" data-items-to-slide="1" data-as-nav-for=".cover-photos-slider .slides-list">

		<ul class="clean-list carousel-items"><?php

			foreach ( $attachment_ids as $attachment_id ) {

				$image_link = wp_get_attachment_url( $attachment_id );

				if ( ! $image_link )
					continue;

				$image_class = '';
				$image_title 	= esc_attr( get_the_title( $attachment_id ) );
				$image_caption 	= esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );
				$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
					'title'	=> $image_title,
					'alt'	=> $image_title
				) );

				echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<li class="carousel-item"><div class="image">%s</div></li>', $image), $attachment_id, $post->ID, $image_class );

			} ?>

		</ul>

	</div>

	<?php
}