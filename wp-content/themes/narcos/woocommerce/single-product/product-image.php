<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;

?>
<div class="col-md-12">
	<!-- Product Images -->
	<div class="photo-slider">

        <?php if ( has_post_thumbnail() ) {

            $product_images_id = array();
            $product_images_id[] = get_post_thumbnail_id();

            if( count( $product->get_gallery_attachment_ids() ) > 0 )

                $product_images_id = array_merge( $product_images_id, $product->get_gallery_attachment_ids() ) ?>

            <div class="tt-slider cover-photos-slider" data-fade="true" data-speed="700" data-dots="false" data-arrows="false" data-infinite="true" data-as-nav-for=".nav-carousel .carousel-items">

                <ul class="clean-list slides-list">

                    <?php
                    foreach( $product_images_id as $product_image_id ):

                        $image_title 	= esc_attr( get_the_title( $product_image_id ) );
                        $image_caption 	= esc_attr( get_post_field( 'post_excerpt', $product_image_id ) );

                        $image       = wp_get_attachment_image( $product_image_id , 'shop_single', 0, $attr = array(
                            'title' => $image_caption,
                            'alt'   => $image_title
                        ) );

                        printf( '<li class="slide">%s</li>', $image );

                    endforeach
                    ?>

                </ul>

            </div>

            <?php

        } else {

            echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), esc_html__( 'Placeholder','narcos' ) ), $post->ID );

        }
        ?>

		<?php do_action( 'woocommerce_product_thumbnails' ); ?>

	</div>

</div>
