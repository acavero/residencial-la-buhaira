<?php
/**
 * Checkout coupon form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! WC()->cart->coupons_enabled() ) {
	return;
}
?>
<div class="col-sm-12">
	<div class="checkout-heading-block">
		<h5 class="heading"><?php esc_html_e('Have a coupon?','narcos');?> <span><?php esc_html_e('Click here to enter your code','narcos');?></span></h5>

		<form class="checkout-form coupon-form" method="post">
			<input type="text" name="coupon_code" class="form-input check-value" placeholder="<?php esc_attr_e( 'Coupon code', 'narcos' ); ?>" id="coupon_code" value="" />
			<input type="submit" class="button apply-coupon" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'narcos' ); ?>" />
		</form>
	</div>
</div>
