<?php 
	$page_id = narcos_get_page_id();
	$blog_bg = get_post_meta( $page_id, THEME_NAME . '_page_background', true );
	
	if(is_single() && has_post_thumbnail($page_id)) {
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'large-featured-image');
		$blog_bg = $thumb[0];
	} elseif(is_single() && !has_post_thumbnail($page_id)) {
		$blog_bg = '';
	} elseif($blog_bg) {
		$blog_bg = $blog_bg['url'];
	} else {
		$blog_bg = '';
	}
?>
<!-- Intro Box -->
<div class="intro-box <?php print is_single() ? 'single-post ' : ''; if(empty($blog_bg)) print 'no-image-post';?>" data-parallax-bg="<?php echo esc_attr($blog_bg);?>">
	<div class="box-img-wrapper">
		<div class="box-img">
			<span></span>
		</div>
	</div>

	<div class="intro-box-inner container">
		<div class="box-content align-center">
			<h2 class="page-title">
				<?php if (is_category()) { ?><?php esc_html_e('Category:', 'narcos'); ?> <strong><?php single_cat_title(); ?></strong>
                <?php } elseif( is_tag() ) { ?><?php esc_html_e('Post Tagged with:', 'narcos'); ?> <strong>"<?php single_tag_title(); ?>"</strong>
                <?php } elseif (is_day()) { ?><?php esc_html_e('Archive for: ', 'narcos'); ?> <strong><?php the_time('F jS, Y'); ?></strong>
                <?php } elseif (is_month()) { ?><?php esc_html_e('Archive for: ', 'narcos'); ?> <strong><?php the_time('F, Y'); ?></strong>
                <?php } elseif (is_year()) { ?><?php esc_html_e('Archive for: ', 'narcos'); ?> <strong><?php the_time('Y'); ?></strong>
                <?php } elseif (is_author()) { ?><?php esc_html_e('Author Archives: ', 'narcos'); echo '<strong>'.get_the_author().'</strong>'; ?>  
                <?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?><?php esc_html_e('Archives', 'narcos');?>
                <?php } elseif (is_search()) { ?><?php esc_html_e('Results for: ','narcos'); ?><?php echo get_search_query(); ?>
                <?php } elseif (is_home()) { ?><?php esc_html_e('Home','narcos'); ?>
                <?php } else { ?><?php echo get_the_title($page_id);?><?php } ?>
			</h2>
			<?php echo narcos_breadcrumbs();?>
		</div>
	</div>

	<!-- Scroll Down Btn -->
	<a href="#" class="scroll-btn">
		<span class="icon"></span>
	</a>

	<!-- Rounded Border Bottom -->
	<div class="rounded-border-bottom">
		<span></span>
	</div>
</div>