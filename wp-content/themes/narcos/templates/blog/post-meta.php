<?php $author_id = get_the_author_meta( 'ID' ); ?>
<div class="blog-post-body">
	<ul class="clean-list post-meta">
		<li><a href="<?php echo get_author_posts_url( $author_id ); ?>"><?php the_author(); ?></a></li>
		<li><?php the_time(get_option('date_format'));?></li>
		<li><?php the_category();?></li>
	</ul>

	<h3 class="post-title">
		<a href="<?php the_permalink();?>"><?php the_title();?></a>
	</h3>

	<p class="post-excerpt"><?php print narcos_excerpt(get_the_ID(), 185);?></p>

	<a href="<?php the_permalink();?>" class="btn template-btn-1"><?php esc_html_e('Seguir Leyendo','narcos');?></a>
</div>