<?php $post_id = get_the_ID();
	  $class = !has_post_thumbnail() || empty($audio_meta) ? 'no-image-post' : '';
	  $audio_meta = get_post_meta($post_id , THEME_NAME . '_audio_url', true);
	  ?>

<article <?php post_class('blog-post '.$class);?>>
	<div class="blog-post-cover">
		<?php if(!empty($audio_meta)):?>
		<div class="embed-responsive embed-responsive-16by9">
			<?php echo apply_filters('the_content',$audio_meta); ?>
		</div>
		<?php elseif(has_post_thumbnail()): ?>
		<a href="<?php the_permalink();?>">
            <?php the_post_thumbnail('featured-image');?>
        </a>
		<?php endif; ?>
	</div>
	<?php get_template_part('templates/blog/post' , 'meta');?>
</article>