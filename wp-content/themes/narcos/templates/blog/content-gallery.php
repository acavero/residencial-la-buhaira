<?php 
$post_id = get_the_ID();
global $post; 
$attachments = get_children( array(
	'post_parent' => get_the_ID(),
	'post_status' => 'inherit',
	'post_type' => 'attachment',
	'post_mime_type' => 'image',
	'order' => 'ASC',
	'orderby' => 'menu_order ID',
	'numberposts' => 10)
);
$class = !empty($attachments) ? '' : 'no-image-post';?>

<article <?php post_class('blog-post '.$class);?>>
	<div class="blog-post-cover">
		<?php if(!empty($attachments)): ?>
		<div class="tt-slider blog-post-cover" data-fade="true" data-dots="false" data-speed="650" data-infinite="true">
			<ul class="clean-list slides-list">
				<?php foreach ( $attachments as $thumb_id => $attachment ) : ?>
					<li class="slide">
						<?php echo wp_get_attachment_image($thumb_id, 'featured-image' ); ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php endif;?>
	</div>

	<?php get_template_part('templates/blog/post' , 'meta');?>
</article>