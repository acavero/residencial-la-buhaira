<?php $post_id = get_the_ID();
	  $class = has_post_thumbnail() ? '' : 'no-image-post';
	  $video_meta = get_post_meta($post_id , THEME_NAME . '_video_url', true);
	  ?>

<article <?php post_class('blog-post video-post '.$class);?> data-video="<?php echo esc_attr($video_meta);?>">
	<div class="blog-post-cover">
		<?php if(!empty($video_meta)):?>
		<span class="play-btn-toggle video-toggle">
			<i class="icon-play2"></i>
		</span>
		<?php endif;?>

		<?php if(has_post_thumbnail())
	        the_post_thumbnail( 'featured-image' );?>
	</div>

	<?php get_template_part('templates/blog/post' , 'meta');?>
</article>