<?php $post_id = get_the_ID();
	  $class = has_post_thumbnail() ? '' : 'no-image-post';?>

<article <?php post_class('blog-post '.$class);?>>
	<div class="blog-post-cover">
		<?php if(has_post_thumbnail()):?>
	        <a href="<?php the_permalink();?>">
	            <?php the_post_thumbnail('featured-image');?>
	        </a>
	    <?php endif;?>
	</div>

	<?php get_template_part('templates/blog/post' , 'meta');?>
</article>