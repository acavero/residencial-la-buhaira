<?php 
	$page_id = narcos_get_page_id();
	$video_show = get_post_meta( $page_id, THEME_NAME . '_video_show', true );
	$video_check = get_post_meta( $page_id, THEME_NAME . '_video_check', true );
	$video_bg = get_post_meta( $page_id, THEME_NAME . '_video_bg', true );
	$video_embed = get_post_meta( $page_id, THEME_NAME . '_video_embed', true );

	if($video_check) {
		$page_bg = !empty($video_bg) ? $video_bg['url'] : '';
		$page_embed = $video_embed;
	} elseif(_go('show_video_box')) {
		$page_bg = _go('video_background');
		$page_embed = _go('video_embed');
	}
?>

<?php if(_go('show_video_box'))
	if($video_show):?>
	<!-- Video Box -->
	<div class="video-box" data-parallax-bg="<?php echo esc_attr($page_bg);?>" data-embed='<?php print $page_embed;?>'>
		<div class="box-img-wrapper">
			<div class="box-img">
				<span></span>
			</div>
		</div>

		<div class="video-toggle-wrapper align-center">
			<span class="video-toggle">
				<i class="icon-video-icon"></i>
			</span>
		</div>
	</div>
<?php endif;?>
