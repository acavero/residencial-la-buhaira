<?php
define('THEME_NAME', 'narcos');
define('THEME_PRETTY_NAME', 'Narcos');

//Load Textdomain
add_action('after_setup_theme', 'tt_theme_textdomain_setup');
function tt_theme_textdomain_setup(){
	if(load_theme_textdomain('narcos', get_template_directory() . '/languages'))
		define('TT_TEXTDOMAIN_LOADED',true);
}

//content width
if (!isset($content_width))
    $content_width = 1170;

//============Theme support=======
//post-thumbnails
add_theme_support('post-thumbnails');

function tt_add_thumb_sizes() {
    add_image_size( 'featured-image', 972, 450, true );
    add_image_size( 'large-featured-image', 1853, 466, true );
    add_image_size( 'portfolio-featured', 900, 550, true );
    add_image_size( 'portfolio-featured-boxed', 480, 480, true );
}
add_action( 'after_setup_theme', 'tt_add_thumb_sizes' );
//add feed support
add_theme_support('automatic-feed-links');
//add  woocommerce
add_theme_support('woocommerce');
//add  html5 support
add_theme_support('html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'));
//add  postformats support
add_theme_support('post-formats', array('gallery','video','audio','image'));