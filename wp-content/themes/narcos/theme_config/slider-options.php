<?php

return array(
	'tt_portfolio' => array(
		'name' => 'Portfolio',
		'term' => 'Portfolio',
		'term_plural' => 'Items in Portfolio',
		'order' => 'DESC',
		'has_single' => true,
		'post_options' => array(
			'supports'=> array( 'title','editor','thumbnail', 'comments'),
			'taxonomies' => array('post_tag'),
			'has_archive'=>true
			),
		'taxonomy_options' => array('show_ui' => true),
		'options' => array(),
		'icon' => 'icons/favicon.png',
		'output' => array(
			'main' => array(
				'shortcode' => 'tt_portfolio',
				'view' => 'views/portfolio-view',
				'shortcode_defaults' => array(
					'item_category' => '',
					'columns' => '',
					'nr' =>	'5000',
					'offset' => '',
					'wide_layout' => '',
					'box_style' => '',
					'hide_filters' => '',
					'el_class' => '',
					'css' => ''
				)
			),
		),

		'single' => array(
			'view' => 'views/portfolio-single',
			'shortcode_defaults' => array(

			)
		),
	),

	'testimonials' => array(
		'name' => 'Testimonials',
		'term' => 'testimonial',
		'term_plural' => 'testimonials',
		'order' => 'ASC',
		'has_single' => false,
		'post_options' => array('supports'=>array('title')),
		'taxonomy_options' => array('show_ui'=>true),
		'options' => array(
			'testimonial_text' => array(
				'type' => 'text',
				'title' => 'Testimonial Message',
				'description' => 'Insert testimonial message.',
			),
			'testimonial_position' => array(
				'type' => 'line',
				'title' => 'Position',
				'description' => 'Position of the team member'
			),
		),
		'icon' => 'icons/favicon.png',
		'output_default' => 'main',
		'output' => array(
			'main' => array(
				'shortcode' => 'tt_testimonials',
				'view' => 'views/testimonials-view',
				'shortcode_defaults' => array(	
					'category' => '',
					'el_class' => '',
					'css' => '',
					'nr' => 5000
				)
			),			
		)
	),
);