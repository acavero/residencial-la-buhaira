<div class="input-line">
	<input data-parsley-errors-container="#results" type="text" class="form-input check-value" id="<?php echo esc_attr($name)?>" name="<?php echo esc_attr($name)?>" placeholder="<?php echo esc_attr($placeholder)?>" data-parsley-error-message="<?php echo esc_attr($label) . esc_html__(' is equired','narcos');?>" <?php if(!empty($required)) echo 'data-parsley-required="true"'; ?>>
	<span class="label" data-name="<?php echo esc_attr($label)?>"></span>
</div>