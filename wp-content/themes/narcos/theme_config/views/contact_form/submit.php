<input 
	type="submit"
    class="btn template-btn-1"
 	data-sending='<?php esc_html_e('Enviando Mensaje','narcos') ?>'
	data-sent='<?php esc_html_e('Mensaje enviado correctamente','narcos') ?>'
	value="<?php echo isset($label) && $label !== '' ? $label : 'Send' ?>">