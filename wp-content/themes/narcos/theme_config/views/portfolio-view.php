<?php
$nr = $shortcode['nr'];
$category = $shortcode['item_category'] ? '.'.$shortcode['item_category'] : '*';
$column = $shortcode['columns'] ? $shortcode['columns'] : 'col-md-12';
$box_style = $shortcode['box_style'] ? $shortcode['box_style'] : 'style-1';
$layout = $shortcode['wide_layout'];
$filters = $shortcode['hide_filters'];
$el_class = $shortcode['el_class'];
$css_class = !empty($shortcode['css']) ? vc_shortcode_custom_css_class( $shortcode['css'] ) : '';

if(!empty($shortcode['offset']))
	$slides = array_slice($slides,$shortcode['offset']);
?>

<div class="section-portfolio-extended <?php echo esc_attr($css_class.' '.$el_class); print $filters ? ' no-padding' : '';?>">
	<?php if(!$filters):?>
	<div class="portfolio-filters isotope-filters">
		<ul class="clean-list">
			<li><a data-filter="*" href="#" <?php if($category == '*') echo 'class="current"';?>><?php esc_html_e('All','narcos');?></a></li>
			<?php foreach($all_categories as $category_slug => $category_name): ?>
	            <li><a href="#" data-filter=".<?php echo esc_attr($category_slug); ?>" <?php if('.'.$category_slug==$category) echo 'class="current"';?>><?php print $category_name; ?></a></li>
	        <?php endforeach; ?>
		</ul>
	</div>
	<?php endif;?>

	<!-- Portfolio Wrapper -->
	<div class="portfolio-wrapper">
		<div class="row isotope-container <?php if($layout) print 'row-fit';?>" data-default-selection="<?php echo esc_attr($category);?>">
			<?php foreach ($slides as $slide_nr => $slide) : if($slide_nr >= $shortcode['nr']) break; ?>
			<div class="<?php echo esc_attr($column);?> isotope-item <?php echo implode(' ', array_keys($slide['categories'])); ?>">
				<?php if($box_style == 'style-1'):?>
				<div class="portfolio-box">
					<div class="box-cover">
						<div class="box-info">
							<h4 class="portfolio-item-title"><?php echo get_the_title($slide['post']->ID);?></h4>

							<ul class="clean-list portfolio-item-categories">
							<?php 
								if(!empty($slide['categories']))
								foreach($slide['categories'] as $category_slug => $category_name)
									print '<li>'.$category_name.'</li>'; 
							?>
							</ul>
						</div>
						<?php $cover = wp_get_attachment_image_src( get_post_thumbnail_id( $slide['post']->ID ), 'portfolio-featured' , false );?>
						<a href="<?php echo get_the_permalink($slide['post']->ID);?>">
							<img src="<?php echo esc_url($cover[0]);?>" alt="<?php echo get_the_title($slide['post']->ID);?>" />
						</a>
					</div>
				</div>
				<?php else: ?>
				<div class="portfolio-item">
					<div class="hover-effect">
						<div class="effect-inner">
							<a href="<?php echo get_the_permalink($slide['post']->ID);?>">
								<i class="icon"></i>
								<span class="project-link"><?php esc_html_e('View project','narcos');?></span>
							</a>
						</div>
					</div>
					<?php $cover = wp_get_attachment_image_src( get_post_thumbnail_id( $slide['post']->ID ), 'portfolio-featured-boxed' , false );?>
					<img src="<?php echo esc_url($cover[0]);?>" alt="<?php echo get_the_title($slide['post']->ID);?>" />
				</div>
				<?php endif;?>
			</div>
			<?php endforeach;?>
		</div>
	</div>
</div>