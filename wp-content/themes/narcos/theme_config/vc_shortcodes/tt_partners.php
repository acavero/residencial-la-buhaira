<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$items_array = (array) vc_param_group_parse_atts( $partners_list );?>

<div class="tt-carousel partners-carousel <?php print $css_class.' '.$el_class;?>" data-items-desktop="5" data-items-small-desktop="4" data-items-tablet="3" data-items-phone="2" data-infinite="true" data-dots="false" data-arrows="false" data-items-to-slide="1">
	<ul class="clean-list carousel-items">
		<?php if(!empty($items_array))
		foreach ($items_array as $key => $tab) {
			$url = vc_build_link( $tab['link'] );
			$output = '<li class="carousel-item"><div class="partner-box">';
			$output .= $url['url'] ? '<a href="'.esc_url($url['url']).'" title="'.esc_attr($url['title']).'" target="'.( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self').'">' : '';
			$output .= '<img src="'.wp_get_attachment_url($tab['item_image']).'" alt="'.$tab['item_title'].'" />';
			$output .= $url['url'] ? '</a>' : '';
			$output .= '</div></li>';
			print balanceTags($output);
			$output = '';
		} ?>
	</ul>
</div>