<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';

$col = $columns;

if($post_style == 'style_2' && $col == 'col-md-8')
	$col = 'col-md-7';

$args = array(
    'post_status'   =>  'publish',
    'post_type'     =>  'post',
    'showposts' => $nr_posts ? $nr_posts : 2,
);

if(!empty($nr_posts_offset))
    $args['offset'] = $nr_posts_offset;

if(!empty($tesla_category))
    $args['cat'] = $tesla_category;

$tt_query = new WP_Query($args); ?>

<div class="tt-blog-feed <?php print $css_class.' '.$el_class;?>">
	<div class="row">
		<?php while($tt_query->have_posts()): $tt_query->the_post(); ?>
		<div class="<?php print $col == 'col-md-7' && $tt_query->current_post == 1 ? 'col-md-10' : $col;?>">
			<?php if($post_style == 'style_1'):?>
			<div class="news-item">
				<?php if(has_post_thumbnail()): ?>
				<div class="item-cover">
					<a href="<?php the_permalink();?>">
						<?php the_post_thumbnail('featured-image');?>
					</a>
				</div>
				<?php endif;?>


				<div class="item-body align-center">
					<h3 class="item-title">
						<a href="<?php the_permalink();?>"><?php the_title();?></a>
					</h3>

					<ul class="clean-list item-meta">
						<li><?php the_time(get_option('date_format'));?></li>
						
					</ul>

					<p class="item-excerpt"><?php print narcos_excerpt(get_the_ID(), 340);?></p>

					<a href="<?php the_permalink();?>" class="btn template-btn-1"><?php esc_html_e('Seguir Leyendo','narcos');?></a>
				</div>
			</div>
			<?php elseif($post_style == 'style_2'):?>
			<?php if($col == 'col-md-7') print '<div class="blog-post-preview-wrapper">';?>
				<div class="blog-post-preview <?php print $tt_query->current_post == 1 ? 'featured' : '';?>">
					<div class="post-cover">
						<ul class="clean-list post-meta">
							<li><?php the_author(); ?></li>
							<li><?php the_time(get_option('date_format'));?></li>
							<li><?php the_category(',');?></li>
						</ul>
						<?php 
							if($tt_query->current_post !== 1)
								the_post_thumbnail('featured-image');
						?>
					<?php print $tt_query->current_post == 1 ? '' : '</div>';?>

					<div class="post-body align-center">
						<h3 class="post-title">
							<a href="<?php the_permalink();?>"><?php the_title();?></a>
						</h3>

						<p class="post-excerpt"><?php print narcos_excerpt(get_the_ID(), $tt_query->current_post == 1 ? 600 : 290 );?></p>

						<a href="<?php the_permalink();?>" class="btn template-btn-1"><?php esc_html_e('Read more','narcos');?></a>
					</div>
					<?php 
						if($tt_query->current_post == 1)
							the_post_thumbnail('featured-image');
					?>

					<?php print $tt_query->current_post == 1 ? '</div>' : '';?>
				</div>
			<?php if($col == 'col-md-7') print '</div>';?>
			<?php endif;?>
		</div>
	    <?php endwhile; wp_reset_postdata(); ?>
	</div>
</div>