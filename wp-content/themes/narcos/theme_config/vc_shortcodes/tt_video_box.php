<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
?>

<div class="video-box <?php print $css_class.' '.$el_class;?>" data-parallax-bg="<?php echo wp_get_attachment_url($background);?>" data-embed='<?php print $video_url;?>'>
	<div class="box-img-wrapper">
		<div class="box-img">
			<span></span>
		</div>
	</div>

	<div class="video-toggle-wrapper align-center">
		<span class="video-toggle">
			<i class="icon-video-icon"></i>
		</span>
	</div>
</div>