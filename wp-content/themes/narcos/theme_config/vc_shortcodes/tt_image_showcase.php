<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : ''; ?>

<div class="showcase-image-wrapper centered macbook-showcase <?php print $el_class.' '.$css_class;?>">
	<div class="image-block">
		<div class="block-content" style="<?php print $showcase_settings;?>">
			<div class="inner-content">
				<img src="<?php echo wp_get_attachment_url( $showcase_img );?>" alt="<?php esc_html_e('showcase img','narcos');?>">
			</div>
		</div>
		<img src="<?php echo wp_get_attachment_url( $showcase_bg );?>" class="image-block-base" alt="<?php esc_html_e('showcase img','narcos');?>">
	</div>
</div>