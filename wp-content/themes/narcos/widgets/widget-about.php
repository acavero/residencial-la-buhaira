<?php

class Tesla_about_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
				'tesla_about_widget',
				'['.THEME_PRETTY_NAME.'] About',
				array(
					'description' => esc_html__('Displays website info', 'narcos'),
					'classname' => 'widget_about',
				)
		);
	}

	function widget($args, $instance) {
		extract($args);
		$description = $instance['description'];
		$email = $instance['email'];
		$phone = $instance['phone'];

		print $before_widget;
		?>

			<a href="<?php echo home_url('/'); ?>" style="<?php _estyle_changer('logo_text') ?>" >
		        <?php if(_go('logo_text')): ?>
		            <?php _eo('logo_text') ?>
		        <?php elseif(_go('logo_image')): ?>
		            <img src="<?php _eo('logo_image') ?>" alt="<?php echo THEME_PRETTY_NAME ?> logo">
		        <?php else: ?>
		            <?php echo THEME_PRETTY_NAME; ?>
		        <?php endif; ?>
		    </a>

			<p><?php print $description;?></p>

			<ul class="clean-list contact-info">
				<?php 
				if(!empty($email))
					echo sprintf('<li class="mail"><a href="mailto:%s">%s</a></li>',$email, $email);
				if(!empty($phone))
					echo sprintf('<li class="phone">%s</li>', $phone);
				?>
			</ul>

		<?php
		print $after_widget;
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['description'] = $new_instance['description'];
		$instance['email'] = $new_instance['email'];
		$instance['phone'] = $new_instance['phone'];

		return $instance;
	}

	function form($instance) {
		$description = isset( $instance['description'] ) ? $instance['description'] : '';
		$email = isset( $instance['email'] ) ? $instance['email'] : '';
		$phone = isset( $instance['phone'] ) ? $instance['phone'] : '';
		?>
		<p>
			<label><?php esc_html_e('Description:','narcos'); ?><input class="widefat" name="<?php echo esc_attr($this->get_field_name('description')); ?>" type="text" value="<?php echo esc_attr($description); ?>" /></label>
		</p>
		<p>
			<label><?php esc_html_e('Email:','narcos'); ?><input class="widefat" name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text" value="<?php echo esc_attr($email); ?>" /></label>
		</p>
		<p>
			<label><?php esc_html_e('Phone:','narcos'); ?><input class="widefat" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" type="text" value="<?php echo esc_attr($phone); ?>" /></label>
		</p>
		<?php
	}
}

add_action('widgets_init', create_function('', 'return register_widget("Tesla_about_widget");'));