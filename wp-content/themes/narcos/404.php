<?php get_header();?>

<?php
/**
 * 404 Page
 */

$background = _go('error_background') ? _go('error_background') : get_template_directory_uri().'/img/intro-box-404.jpg';
?>

<!-- Intro Box -->
<div class="intro-box no-scroll" data-parallax-bg="<?php echo esc_attr($background);?>">
	<div class="box-img-wrapper">
		<div class="box-img">
			<span></span>
		</div>
	</div>

	<div class="error-intro">
		<div class="row">
			<div class="col-lg-16 col-lg-offset-4 col-md-18 col-md-offset-3">
				<div class="intro-error-wrapper">
					<?php printf('<h1>%s<span>%s</span></h1>', _go('error_title') ? _go('error_title') : esc_html__('Error', 'narcos'), _go('error_subtitle') ? _go('error_subtitle') : esc_html__('404', 'narcos') ); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer();?>