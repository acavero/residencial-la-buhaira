<?php get_header(); ?>	

<?php
/**
 * Index Page
 */
$page_id = narcos_get_page_id();
$sidebar = get_post_meta( $page_id, THEME_NAME . '_sidebar_position', true );
$rev_slider = get_post_meta( $page_id, THEME_NAME . '_rev_slider_alias', true );
$sidebar = $sidebar ? $sidebar : 'right';
?>

<?php if(empty($rev_slider))
	get_template_part('templates/intro','box');
else
	echo do_shortcode($rev_slider);
?>

<!-- Blog Section -->
<section class="section section-blog no-margin">
	<div class="row">
		<?php if($sidebar == "left"): ?>
		<div class="col-lg-8 col-lg-offset-2 col-md-9">
			<?php get_sidebar();?>
		</div>
		<?php endif;?>

		<div class="<?php print $sidebar == "full_width" ? 'col-lg-12 col-lg-offset-6 col-md-18 col-md-offset-3' : 'col-lg-12 col-lg-offset-2 col-md-15';?>">
			<?php if (have_posts()): ?>	
				<div class="blog-list">		
					<?php while(have_posts()): the_post(); 
						get_template_part('templates/blog/content',get_post_format( ));
					endwhile; ?>
				</div>
				<?php get_template_part('templates/nav','main'); ?>
			<?php endif; ?>
		</div>

		<?php if($sidebar == "right"): ?>
		<div class="col-lg-8 col-lg-offset-2 col-md-9">
			<?php get_sidebar();?>
		</div>
		<?php endif;?>
	</div>
</section>
<?php get_footer();?>