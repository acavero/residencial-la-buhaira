<?php get_header();
/**
 * Single Portfolio Page
 */

$page_id = narcos_get_page_id();
$sidebar = get_post_meta( $page_id, THEME_NAME . '_sidebar_position', true );
$background = get_post_meta( $page_id, THEME_NAME . '_page_background', true );
$background = !empty($background) ? 'data-parallax-bg="'.$background['url'].'"' : '';
$sidebar = empty($sidebar) ? 'right' : $sidebar;
?>

<?php while ( have_posts() ) : the_post(); 
$terms = get_the_terms( get_the_ID(),'tt_portfolio_tax'); ?>
	<!-- Intro Box -->
	<div class="intro-box style-2 <?php print empty($background) ? 'no-portfolio-bg' : '';?>" <?php print $background;?>>
		<div class="box-img-wrapper">
			<div class="box-img">
				<span></span>
			</div>
		</div>

		<div class="intro-box-inner">
			<div class="box-content">
				<div class="project-info">
					<h2 class="project-title"><?php the_title();?></h2>
					<p class="project-meta"><span><?php the_time(get_option('date_format'));?> | <?php if(!is_wp_error($terms)) { foreach($terms as $term) print $term->name; } ?></p>
				</div>
				<?php echo narcos_breadcrumbs();?>
			</div>
		</div>
	</div>

	<!-- Blog Section -->
	<section class="section single-project-view <?php print $sidebar == 'full_width' ? '' : 'section-blog no-margin';?>">
		<div class="row">
			<?php if($sidebar == "left"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>

			<div class="<?php print $sidebar == "full_width" ? 'col-lg-16 col-lg-offset-4 col-md-18 col-md-offset-3' : 'col-lg-12 col-lg-offset-2 col-md-15';?>">
				<div class="project-cover">
					<?php the_post_thumbnail();?>
				</div>


				<div class="project-body">
					<?php the_content();?>
					<div class="post-navigation">
			            <?php wp_link_pages(array(
			                'next_or_number'   => 'number',
			                'nextpagelink'     => esc_html__( 'Next page','narcos' ),
			                'previouspagelink' => esc_html__( 'Previous page','narcos' ),
			                'pagelink'         => '%',
			                'echo' => 1
			            )); ?>
			        </div>
				</div>

				<div class="project-footer">
					<div class="share-block-wrapper align-center">
						<?php narcos_share(); ?>
					</div>

					<div class="row">
						<div class="col-sm-8 col-xs-12">
							<div class="nav-item prev">
								<a href="<?php echo get_permalink(get_adjacent_post(false,'',true)); ?>">
									<span class="heading"><?php esc_html_e('Prev item','narcos');?></span>
								</a>
							</div>
						</div>

						<div class="col-sm-8 col-xs-12 col-sm-offset-8">
							<div class="nav-item next">
								<a href="<?php echo get_permalink(get_adjacent_post(false,'',false)); ?>">
									<span class="heading"><?php esc_html_e('Next item','narcos');?></span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php if($sidebar == "right"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>
		</div>
	</section>

	<?php comments_template();?>

<?php endwhile; ?>
<?php get_footer();?>