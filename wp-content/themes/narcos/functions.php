<?php 
/***********************************************************************************************/
/*  Tesla Framework */
/***********************************************************************************************/
require_once(get_template_directory() . '/tesla_framework/tesla.php');

if ( is_admin() && current_user_can( 'install_themes' ) ) {
    require_once( get_template_directory() . '/plugins/tgm-plugin-activation/register-plugins.php' );
}

/***********************************************************************************************/
/* Google fonts + Fonts changer */
/***********************************************************************************************/
TT_ENQUEUE::$gfont_changer = array(
    _go('global_typo_font'),
    _go('links_settings_font'),
    _go('logo_text_font'),
    _go('headings_settings_font')
);

TT_ENQUEUE::$base_gfonts = array('Noto Sans:400,400italic,700,700italic|Satisfy');
TT_ENQUEUE::add_js(array('http://maps.googleapis.com/maps/api/js'));

function narcos_to_js() {
    $send_js = array(
        'dirUri' => get_template_directory_uri()
    );
    wp_localize_script( 'options.js', 'themeOptions', $send_js );
    wp_register_script( 'g-map'
        , '//maps.googleapis.com/maps/api/js?libraries=places'
        , array('jquery'), null, true );
}
add_action( 'wp_enqueue_scripts', 'narcos_to_js', 12 );

if ( is_admin() ):

function narcos_admin_js( $hook ) {
    wp_enqueue_script( 'admin-g-map'
        , '//maps.googleapis.com/maps/api/js?libraries=places'
        , array('jquery'), null, true );
}
add_action( 'admin_enqueue_scripts', 'narcos_admin_js', 10, 1);
endif;


/***********************************************************************************************/
/* Custom CSS & Custom JS*/
/***********************************************************************************************/
add_action('wp_enqueue_scripts', 'narcos_custom_css', 99);
function narcos_custom_css() {
    $custom_css = _go('custom_css') ? _go('custom_css') : '';
    wp_add_inline_style('tt-main-style', $custom_css);

    if(function_exists('wp_add_inline_script'))
        wp_add_inline_script('tt-custom-js', _go('custom_js'));
}

/***********************************************************************************************/
/* Add Theme Support */
/***********************************************************************************************/
function narcos_theme_slug_setup() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'narcos_theme_slug_setup' );

function narcos_theme_favicon() {
    if( function_exists( 'wp_site_icon' ) && has_site_icon() ) {
        wp_site_icon();
    } else if(_go( 'favicon_link')){
        echo "\r\n" . sprintf( '<link rel="shortcut icon" href="%s">', _go( 'favicon_link') ) . "\r\n";
    }
}

add_action( 'wp_head', 'narcos_theme_favicon');


/***********************************************************************************************/
/* Add Menus */
/***********************************************************************************************/

function narcos_register_menus($return = false){
    $tt_menus = array(
            'main_menu'    => esc_html_x('Boxed menu', 'dashboard','narcos'),
            'secondary_menu'    => esc_html_x('Expanded menu', 'dashboard','narcos'),
        );
    if($return){
        return array('main_menu' => 'Main Menu','secondary_menu' => 'Expanded Menu');
    }
    register_nav_menus($tt_menus);
}
add_action('init', 'narcos_register_menus');


/***********************************************************************************************/
/* Custom Functions */
/***********************************************************************************************/
function narcos_excerpt( $id, $length = NULL ) {
    $length = !empty($length) ? $length : 55;
    $content = apply_filters( 'the_content', get_post_field('post_content', $id));
    $content = strip_shortcodes($content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $content = strip_tags($content);
    $content = substr($content, 0, $length);
    
    return $content.'...';
}

function narcos_load_composer() {
    if (class_exists('Vc_Manager')) {
        vc_set_shortcodes_templates_dir(TEMPLATEPATH . '/theme_config/vc_shortcodes');
        require_once(TEMPLATEPATH . '/theme_config/vc_shortcodes/theme_shortcodes.php');
        require_once(TEMPLATEPATH . '/theme_config/vc_shortcodes/rewrite_map.php');
        include_once(TEMPLATEPATH . '/theme_config/vc_shortcodes/params/google_map.php' );
    }
}
add_action('init','narcos_load_composer');

function narcos_breadcrumbs() {
    global $post;

    $links = array();
    
    if(is_home()) {
        $page_id = get_option( 'page_for_posts' );
        $links[] = get_the_title($page_id); 

    } else if(tesla_has_woocommerce()) {
        if(is_shop()) {
            $page_id = get_option( 'woocommerce_shop_page_id' );
            $links[] = get_the_title($page_id);  
        } else if(is_product()) {
            $page_id = get_option( 'woocommerce_shop_page_id' );
            ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
            $links[] = get_the_title();
        } else if(is_product_category() || is_product_tag()) {
            $page_id = get_option( 'woocommerce_shop_page_id' );
            ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
            $links[] = get_the_archive_title();
        } 

    }  else if(is_singular('post')) {
        $page_id = get_option( 'page_for_posts' );
        ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
        $links[] = get_the_title();
    } else if(is_singular('tt_portfolio')) {
        $page_id = '';
        $links[] = get_the_title();
    } else if(is_tag()) {
        $page_id = get_option( 'page_for_posts' );
        ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
        $links[] = single_tag_title('', false); 
    } else if(is_category()) {
        $page_id = get_option( 'page_for_posts' );
        ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
        $links[] = single_cat_title("", false);
    } else if(is_archive()) {
        $page_id = get_option( 'page_for_posts' );
        ($page_id !== '0') ? $links[get_permalink($page_id)] = get_the_title($page_id) : '';
        $links[] = get_the_archive_title();
    } else if(is_search()) {
        $page_id = get_option( 'page_for_posts' );
        $links[] = esc_html__('Search', 'narcos');
    } else if(is_page()) {

        if($post->post_parent > 0) {
            $page_id = $post->post_parent;
            if($page_id !== (int) get_option('page_on_front')) {
                $links[get_permalink($page_id)] = get_the_title($page_id);                    
            }
        }
        $links[] = get_the_title();
    }

    ob_start(); ?>
    <ul class="breadcrumb clean-list">
        <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo get_option('page_on_front') ? get_the_title(get_option('page_on_front')) : esc_html__('Home','narcos');?></a></li>
        <?php foreach ($links as $key => $link) {
            if(is_string($key)) {
                echo sprintf('<li><a href="%s">%s</a></li>', esc_url($key), esc_html($link));
            } else {
                echo sprintf('<li>%s</li>', esc_html($link));                               
            }
        } ?>
    </ul>
    <?php return ob_get_clean();
}

function narcos_get_page_id($shop = false){
    global $wp_query;
    $page_id = get_the_ID();
    if(is_post_type_archive('post') || is_archive() || is_search())
        return get_option('page_for_posts');

    if(tesla_has_woocommerce())
        if(is_woocommerce())
            return get_option( 'woocommerce_shop_page_id' );

    if(get_query_var('page_id'))
        $page_id = get_query_var('page_id');
    elseif(!empty($wp_query->queried_object) && !empty($wp_query->queried_object->ID))
        $page_id = $wp_query->queried_object->ID;
    elseif($shop)
        $page_id = get_option( 'woocommerce_shop_page_id' );
    else
        $page_id = false;
    return $page_id;
}

/***********************************************************************************************/
/* Custom Colors */
/***********************************************************************************************/
add_action('wp_enqueue_scripts', 'narcos_theme_custom_css', 99);

function narcos_theme_custom_css() {
    // Main styles switches
        $custom_css  = ( _go( 'layout_style' ) == 'Boxed' ) ? '#page {max-width: 1900px; margin: 0 auto;} ' : '';
        $custom_css .= ( _go( 'body_background' ) || ( _go( 'body_color' ) || _go( 'body_background_repeat' ) || _go( 'body_background_position' ))) ? 'body {' : '';
        $custom_css .= ( _go( 'body_background')) ? 'background-image: url('._go('body_background').'); ' : '';
        $custom_css .= ( _go( 'body_color')) ? 'background-color: '._go('body_color').'; ' : '';
        $custom_css .= ( _go( 'body_background') && _go('body_background_repeat')) ? 'background-repeat: '.strtolower(_go('body_background_repeat')).'; ' : '';
        $custom_css .= ( _go( 'body_background') && _go('body_background_position')) ? 'background-attachment: '.strtolower(_go('body_background_position')).'; ' : '';
        $custom_css .= ( _go( 'body_background') || (_go('body_color') || _go('body_background_repeat') || _go('body_background_position'))) ? '}' : '';
        $custom_css .= _go('body_color') ? ' footer .main-social-block,.blog-post.no-image-post .blog-post-cover {background: '._go('body_color').'} .rounded-border-bottom {color: '._go('body_color').'}' : '';

        // Main styles switches
        $custom_css .= (_go('header_bg')) ? '
        .main-header nav>ul>li.menu-item-has-children>.sub-menu, 
        .main-header nav>ul>li.menu-item-has-children>.children,
        .main-header nav>ul>li.menu-item-has-children>.sub-menu li.menu-item-has-children .sub-menu, 
        .main-header nav>ul>li.menu-item-has-children>.sub-menu li.menu-item-has-children .children, 
        .main-header nav>ul>li.menu-item-has-children>.children li.menu-item-has-children .sub-menu, 
        .main-header nav>ul>li.menu-item-has-children>.children li.menu-item-has-children .children,
        .main-header nav .shopping-cart-wrapper .cart-toggle:hover,
        html:before {
            background-color: '._go('header_bg').' !important;
        }

        .intro-box.no-image-post {
            background: '.adjustBrightness(_go('header_bg'), -30).'; 
        }

        .search-form-wrapper {
            background: '.hex2rgba(_go('header_bg'), 0.8).'; 
        }
        ' : '';

        $custom_css .= (_go('header_text')) ? '
        .main-header nav>ul>li>a,
        .main-header nav>ul>li:before, 
        .main-header nav>ul>li:after,
        .main-header nav>ul>li.menu-item-has-children>.sub-menu li a, 
        .main-header nav>ul>li.menu-item-has-children>.children li a,
        .main-header nav .search-toggle i,
        .main-header nav .shopping-cart-wrapper .cart-toggle span,
        .main-header nav .shopping-cart-wrapper .cart-toggle i,
        .search-form-wrapper .close-search-form-wrapper i,
        .search-form-wrapper .search-form .form-submit i,
        .search-form-wrapper .search-form .form-input,
        .stack-menu-toggle .menu-toggle-title, 
        .mobile-navigation-toggle .menu-toggle-title,
        html .stack-nav-wrapper .stack-nav>ul>li>a,
        html .stack-nav-wrapper .stack-nav>ul>li ul li,
        .intro-box .intro-box-inner .box-content .page-title,
        .intro-box .intro-box-inner .box-content .breadcrumb li,
        .intro-box.style-2 .intro-box-inner .box-content .project-info .project-title,
        .intro-box.style-2 .intro-box-inner .box-content .project-info .project-meta
        { color: '._go('header_text').'; }

        .main-header nav>ul>li:hover a {
            border-color: '.hex2rgba(_go('header_text'), 0.6).'; 
        }

        .scroll-btn .icon,
        .stack-menu-toggle .icon, .mobile-navigation-toggle .icon {
            border-color: '._go('header_text').'; 
        }

        .scroll-btn .icon:before,
        .stack-menu-toggle .icon .line, .mobile-navigation-toggle .icon .line {
            background: '._go('header_text').'; 
        }

        html .stack-nav-wrapper .close-stack-nav:before, html .stack-nav-wrapper .close-stack-nav:after {
            color: '.adjustBrightness(_go('header_text'), -50).'; 
        }

        .menu-trigger .icon, .menu-trigger .icon:before, .menu-trigger .icon:after {
            background: '._go('header_text').';
        }
        ' : '';

        $custom_css .= (_go('primary_color')) ? '
        p b,
        .comment-form .logged-in-as a:hover,
        .single-post-content .post-navigation p a:hover,
        .woocommerce div.product form.cart .reset_variations:hover {
            color: '.adjustBrightness(_go('primary_color'), -20).';
        }

        .comment-form .form-submit .submit:hover,
        .single-post-content form input[type="submit"]:hover {
            border-color: '.adjustBrightness(_go('primary_color'), -20).';
        }

        .single-post-content form input[type="submit"]:hover {
            background: '.adjustBrightness(_go('primary_color'), -20).';
        }

        blockquote,
        .checkbox.type-2 input:checked + .label:before,
        .tabed-content .tabs-header ul li.current, .tabed-content .tabs-header ul li:hover {
            border-color: '._go('primary_color').';
        }

        .comment-form .rating .rating-box:hover i, 
        .comment-form .rating .rating-box.active i,
        .testimonials-box.v2 .message a,
        .shop-product .product-body a h3:hover,
        .single-product-description-box .box-header .woocommerce-product-rating .star-rating,
        .single-product-description-box .box-body .rating-box i,
        .single-product-description-box .box-body .widget_categories ul li a:before,
        .single-product-description-box .box-body .widget_categories ul li a:hover,
        .section-contact .contact-block-widget .contact-block p a:hover,
        .section-contact .contact-block-widget .contact-block .social-items li i:hover,.content-wrapper .widget:not(.widget_tag_cloud):not(.widget_popular_posts):not(.widget_search):not(.widget_instagram):not(.widget_twitter):not(.widget_popular_products):not(.widget_order_totals):not(.widget_products):not(.widget_top_rated_products):not(.widget_shopping_cart):not(.tesla-twitter-widget):not(.tesla-instagram-widget) ul li a:before,
        .content-wrapper .widget:not(.widget_tag_cloud):not(.widget_popular_posts):not(.widget_search):not(.widget_instagram):not(.widget_twitter):not(.widget_popular_products):not(.widget_order_totals):not(.widget_products):not(.widget_top_rated_products):not(.widget_shopping_cart):not(.tesla-twitter-widget):not(.tesla-instagram-widget) ul li a:hover,
        .content-wrapper .widget.widget_popular_products .price,.content-wrapper .widget.widget_products .product_list_widget li .amount, .content-wrapper .widget.widget_top_rated_products .product_list_widget li .amount, .content-wrapper .widget.widget_shopping_cart .product_list_widget li .amount,
        .comments-list .comment-item .rating-box,
        .comments-list .comment-item .star-rating,
        .comments-list .comment.byuser .rating-box,
        .comments-list .comment.byuser .star-rating,
        .comments-list .comment-item .rating-box i,
        .comments-list .comment-item .star-rating i,
        .comments-list .comment.byuser .rating-box i,
        .comments-list .comment.byuser .star-rating i,
        .quick-view-box .star-rating,
        .media-wrapper .star-rating,
        .share-block .share-options li i,
        .product-widgets .widget_popular_products .popular-products .popular-product .price,
        .content-wrapper .widget.widget_price_filter .price_slider_amount .price_label,
        .single-product-description-box .box-body .widget_categories ul li a:hover:before {
            color: '._go('primary_color').';
        }

        .btn.template-btn-1:after,
        .button.template-btn-1:after,
        .portfolio-filters ul li a:after,
        .single-product-description-box .box-header .product-title:after,
        .section-contact .contact-block-widget .contact-block .block-title:after,
        .content-wrapper .widget .widget-caption:after,
        ul.page-numbers li .page-numbers:after {
            background: '._go('primary_color').';
        }

        .content-wrapper .widget.widget_product_search .woocommerce-product-search input[type="submit"], .content-wrapper .widget.widget_product_search .search-form input[type="submit"], .content-wrapper .widget.widget_search .woocommerce-product-search input[type="submit"], .content-wrapper .widget.widget_search .search-form input[type="submit"] {
            background: '._go('primary_color').';
            border-color: '.adjustBrightness(_go('primary_color'), -20).';
        }

        .content-wrapper .widget.widget_product_search .woocommerce-product-search input[type="submit"]:hover, .content-wrapper .widget.widget_product_search .search-form input[type="submit"]:hover, .content-wrapper .widget.widget_search .woocommerce-product-search input[type="submit"]:hover, .content-wrapper .widget.widget_search .search-form input[type="submit"]:hover {
            background: '.adjustBrightness(_go('primary_color'), -20).';
        }

        .btn.template-btn-1:before, .button.template-btn-1:before,
        .content-wrapper .widget:not(.widget_tag_cloud):not(.widget_popular_posts):not(.widget_search):not(.widget_instagram):not(.widget_twitter):not(.widget_popular_products):not(.widget_order_totals):not(.widget_products):not(.widget_top_rated_products):not(.widget_shopping_cart):not(.tesla-twitter-widget):not(.tesla-instagram-widget) ul li a:hover:before {
            color: '.adjustBrightness(_go('primary_color'), -50).';
        }

        .content-wrapper .widget.widget_price_filter .price-slider .ui-widget-content .ui-slider-range, .content-wrapper .widget.widget_price_filter .price_slider_wrapper .ui-widget-content .ui-slider-range {
            background: '._go('primary_color').' !important;
        }

        .content-wrapper .widget.widget_price_filter .price-slider .ui-widget-content .ui-slider-handle, .content-wrapper .widget.widget_price_filter .price_slider_wrapper .ui-widget-content .ui-slider-handle {
            background: '.adjustBrightness(_go('primary_color'), -40).' !important;
        }
        ' : '';

        $custom_css .= (_go('footer_color')) ? '
        footer .main-footer-wrapper {background-color: '._go('footer_color').' !important;}
        footer .widget_subscribe .subscribe-form .form-input { background: '.adjustBrightness(_go('footer_color'), -20).';}
        footer .widget_subscribe .subscribe-form .form-submit { background: '.adjustBrightness(_go('footer_color'), -50).';}
        footer .widget_subscribe { border-color: '.adjustBrightness(_go('footer_color'), -50).';}
        ' : '';

        $custom_css .= (_go('footer_text')) ? ' 
        footer a, 
        footer .main-footer-wrapper .widget p, 
        footer .main-footer-wrapper .widget ul li,
        footer .main-footer-wrapper .widget .widget-title,
        footer .main-footer-wrapper .copyrights p,
        footer .main-social-block .block-icon {color: '._go('footer_text').';}

        footer .widget_about .contact-info li:before,
        footer .widget_popular_posts .popular-posts .popular-post .post-meta {
            color: '.adjustBrightness(_go('footer_text'), -30).';
        }

        footer .widget_popular_posts .popular-posts .popular-post .post-meta li:not(:first-of-type):before {
            background-color: '.adjustBrightness(_go('footer_text'), -30).';
        }
        ' : '';

        $custom_css .= ( _go('canvas_color' ) ) ? sprintf('#page { background: %s;}', _go('canvas_color')) : '';

        $custom_css .= ( _go('global_typo_color' ) ) ? sprintf('body {color: %s;}', _go('global_typo_color')) : '';
        $custom_css .= ( _go('global_typo_size' ) ) ? sprintf('body {font-size: %spx;}', _go('global_typo_size')) : '';
        $custom_css .= ( _go('global_typo_font' ) ) ? sprintf('body {font-family: %s;}', _go('global_typo_font')) : '';

        $custom_css .= ( _go('links_settings_color' ) ) ? sprintf('a {color: %s;}', _go('links_settings_color')) : '';
        $custom_css .= ( _go('links_settings_size' ) ) ? sprintf('a {font-size: %spx;}', _go('links_settings_size')) : '';
        $custom_css .= ( _go('links_settings_font' ) ) ? sprintf('a {font-family: %s;}', _go('links_settings_font')) : '';

        $custom_css .= ( _go('headings_settings_color' ) ) ? sprintf('h1, h2, h3, h4, h5, h6 {color: %s;}', _go('headings_settings_color')) : '';
        $custom_css .= ( _go('headings_settings_font' ) ) ? sprintf('h1, h2, h3, h4, h5, h6 {font-family: %s;}', _go('headings_settings_font')) : '';
        
        $custom_css .= ( _go('headings_one_settings_size' ) ) ? sprintf( 'h1 {font-size: %spx;}', _go( 'headings_one_settings_size' ) ) : '';
        $custom_css .= ( _go('headings_two_settings_size' ) ) ? sprintf( 'h2 {font-size: %spx;}', _go( 'headings_two_settings_size' ) ) : '';
        $custom_css .= ( _go('headings_three_settings_size' ) ) ? sprintf( 'h3 {font-size: %spx;}', _go( 'headings_three_settings_size' ) ) : '';
        $custom_css .= ( _go('headings_four_settings_size' ) ) ? sprintf( 'h4 {font-size: %spx;}', _go( 'headings_four_settings_size' ) ) : '';
        $custom_css .= ( _go('headings_five_settings_size' ) ) ? sprintf( 'h5 {font-size: %spx;}', _go( 'headings_five_settings_size' ) ) : '';
        $custom_css .= ( _go('headings_six_settings_size' ) ) ? sprintf( 'h6 {font-size: %spx;}', _go(' headings_six_settings_size' ) ) : '';

        $custom_css .= ( _go('custom_css' ) ) ? _go( 'custom_css' ) : '';

        wp_add_inline_style( 'tt-main-style', $custom_css );
}


/***********************************************************************************************/
/* Comments */
/***********************************************************************************************/
 
function narcos_custom_comments( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
    ?>

    <<?php print $tag ?> class="comment-item" id="comment-<?php comment_ID() ?>">
        <?php if ( 'div' != $args['style'] ) : ?>
            <div id="div-comment-<?php comment_ID() ?>" <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?>>
        <?php endif; ?>
        <div class="avatar-cover">
            <?php if ($args['avatar_size'] != 0)
                echo get_avatar( $comment, $args['avatar_size'], false,'avatar image' ); ?>
        </div>

        <div class="comment-body">
            <h3 class="comment-meta"><?php echo get_comment_author_link() ?>, <span><?php echo get_comment_time(get_option('date_format')) ?> <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'],'reply_text'=> esc_html__('reply','narcos') ))) ?></span></h3>

            <p class="comment-content"><?php echo get_comment_text() ?></p>
        </div>
        <?php if ( 'div' != $args['style'] ) : ?>
            </div>
        <?php endif; 

}

/***********************************************************************************************/
/* Add Sidebar Support */
/***********************************************************************************************/
function narcos_register_sidebars(){
    if (function_exists('register_sidebar')) {
        register_sidebar(
            array(
                'name'           => esc_html__('Blog Sidebar', 'narcos'),
                'id'             => 'blog',
                'description'    => esc_html__('Blog Sidebar Area', 'narcos'),
                'before_widget'  => '<div class="widget %2$s">',
                'after_widget'   => '</div>',
                'before_title'   => '<h5 class="widget-caption">',
                'after_title'    => '</h5>'
            )
        );

        register_sidebar(
            array(
                'name'           => esc_html__('Shop Sidebar', 'narcos'),
                'id'             => 'shop',
                'description'    => esc_html__('"Shop" Sidebar Area', 'narcos'),
                'before_widget'  => '<div class="widget %2$s">',
                'after_widget'   => '</div>',
                'before_title'   => '<h5 class="widget-caption">',
                'after_title'    => '</h5>'
            )
        );

        register_sidebar(
            array(
                'name'           => esc_html__('Footer Column 1', 'narcos'),
                'id'             => 'footer_1',
                'description'    => esc_html__('Footer Widget Size 7', 'narcos'),
                'class'         => '',
                'before_widget'  => '<div class="widget %2$s">',
                'after_widget'   => '</div>',
                'before_title'   => '<h5 class="widget-title">',
                'after_title'    => '</h5>'
            )
        );

        register_sidebar(
            array(
                'name'           => esc_html__('Footer Column 2', 'narcos'),
                'id'             => 'footer_2',
                'description'    => esc_html__('Footer Widget Size 3', 'narcos'),
                'class'         => '',
                'before_widget'  => '<div class="widget %2$s">',
                'after_widget'   => '</div>',
                'before_title'   => '<h5 class="widget-title">',
                'after_title'    => '</h5>'
            )
        );

        register_sidebar(
            array(
                'name'           => esc_html__('Footer Column 3', 'narcos'),
                'id'             => 'footer_3',
                'description'    => esc_html__('Footer Widget Size 8', 'narcos'),
                'class'         => '',
                'before_widget'  => '<div class="widget %2$s">',
                'after_widget'   => '</div>',
                'before_title'   => '<h5 class="widget-title">',
                'after_title'    => '</h5>'
            )
        );

        register_sidebar(
            array(
                'name'           => esc_html__('Footer Column 4', 'narcos'),
                'id'             => 'footer_4',
                'description'    => esc_html__('Footer Widget Size 6', 'narcos'),
                'class'         => '',
                'before_widget'  => '<div class="widget %2$s">',
                'after_widget'   => '</div>',
                'before_title'   => '<h5 class="widget-title">',
                'after_title'    => '</h5>'
            )
        );
    }
}
add_action('widgets_init','narcos_register_sidebars');


/***********************************************************************************************/
/* Share Function */
/***********************************************************************************************/
if(!function_exists('narcos_share')){
    function narcos_share(){
        $share_this = _go('share_this');
        if(isset($share_this) && is_array($share_this)): ?>
            <div class="share-block">
                <span class="block-title"><?php esc_html_e('Share this post','narcos');?></span>
                <span class="block-toggle">
                    <i class="icon-share2"></i>
                </span>

                <ul class="clean-list share-options">
                <?php foreach($share_this as $val): ?>
                    <?php if($val === 'googleplus') $val = 'google-plus'; ?>
                        <?php switch ($val) {
                            case 'facebook': ?>
                                    <li>
                                        <a onClick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="icon-<?php echo esc_attr($val );?>"></i></a>
                                    </li>
                                <?php break; ?>
                            <?php case 'twitter': ?>
                                    <li>
                                        <a onClick="window.open('http://twitter.com/intent/tweet?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php the_title(); ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="icon-<?php echo esc_attr($val );?>"></i></a>
                                    </li>
                                <?php break; ?>
                            <?php case 'google-plus': ?>
                                    <li>
                                        <a onClick="window.open('https://plus.google.com/share?url=<?php echo urlencode(get_the_permalink()); ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="icon-<?php echo esc_attr($val );?>"></i></a>
                                    </li>
                                <?php break; ?>
                            <?php case 'pinterest': ?>
                                <li>
                                    <a onClick="window.open('https://www.pinterest.com/pin/create/button/?url=<?php echo urlencode(get_the_permalink()); ?>','sharer','toolbar=0,status=0,width=748,height=325');" href="javascript: void(0)"><i class="icon-<?php echo esc_attr($val );?>"></i></a>
                                </li>
                            <?php break; ?>
                            <?php case 'linkedin': ?>
                                    <li>
                                        <a onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_the_permalink()); ?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><i class="icon-<?php echo esc_attr($val );?>"></i></a>
                                    </li>
                                    <?php break; ?>
                            <?php default: ''; break;
                        } ?>
                <?php endforeach; ?>
                </ul>
            </div>
        <?php endif;
    }
}

/***********************************************************************************************/
/* View count for single posts */
/***********************************************************************************************/

function narcos_set_post_views($postID) {
    $count_key = 'tt_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function narcos_get_post_views($postID) {
    $count_key = 'tt_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}

/**************************************************************************************************************/
/*                                     Woocommerce Functions
****************************************************************************************************************/
function tt_woocommerce_image_dimensions() {
    global $pagenow;

    if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' ) {
        return;
    }
    $catalog = array(
        'width'     => '441',   // px
        'height'    => '550',   // px
        'crop'      => 1        // true
    );

    $single = array(
        'width'     => '755',   // px
        'height'    => '800',   // px
        'crop'      => 1        // true
    );

    $thumbnail = array(
        'width'     => '173',   // px
        'height'    => '190',   // px
        'crop'      => 1        // true
    );
    // Image sizes
    update_option( 'shop_catalog_image_size', $catalog );       // Product category thumbs       
    update_option( 'shop_single_image_size', $single );         // Single product image
    update_option( 'shop_thumbnail_image_size', $thumbnail );   // Image gallery thumbs
}
add_action( 'after_setup_theme', 'tt_woocommerce_image_dimensions', 1 );

if(tesla_has_woocommerce()):
    // Ensure cart contents update when products are added to the cart via AJAX
    add_filter('add_to_cart_fragments', 'tt_woocommerce_header_add_to_cart_fragment');

    function tt_woocommerce_header_add_to_cart_fragment( $fragments ) {
        ob_start(); ?>
            <div class="shopping-cart-wrapper">
                <span class="cart-toggle no-select">
                    <i class="icon-shoppingbag"></i>
                    <span class="cart-items"><?php echo WC()->cart->cart_contents_count;?></span>
                </span>

                <div class="cart-items-wrapper">
                    <?php if(WC()->cart->cart_contents_count): ?>
                    <ul class="clean-list cart-items">
                        <?php foreach(WC()->cart->get_cart() as $cart_item_key => $cart_item):
                            $_product = $cart_item['data'];
                            if ((!apply_filters('woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key) || !$_product->exists() || $cart_item['quantity'] == 0)) { continue; };
                            $product_name  = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
                            $thumbnail     = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image('thumbnail'), $cart_item, $cart_item_key );
                            $product_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                            $product_link  = get_permalink($cart_item['product_id']);
                            ?>
                            <li class="cart-item">
                                <div class="image">
                                    <?php
                                        if ( ! $_product->is_visible() ) {
                                            echo "$thumbnail";
                                        } else {
                                            printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
                                        }
                                        ?>
                                </div>

                                <?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove-product-from-cart" title="%s"><i class="icon-cross"></i></a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), esc_html__( 'Remove this item','narcos' ) ), $cart_item_key ); ?>

                                <h6 class="product-title">
                                     <a href="<?php echo esc_url($product_link); ?>"><?php print $product_name ?></a>
                                </h6>

                                <?php printf( '<span class="meta">%s<sub>%s</sub>%s</span>', $cart_item['quantity'], 'x', $product_price);?>
                                
                            </li>
                        <?php endforeach ?>
                    </ul>

                    <div class="subtotal"><?php echo esc_html__('Subtotal: ','narcos') . WC()->cart->get_cart_subtotal(); ?></div>

                    <ul class="clean-list cart-action clearfix">
                        <li>
                            <a href="<?php echo get_permalink( wc_get_page_id( 'cart' ) ); ?>" class="btn template-btn-1"><?php esc_html_e('view cart','narcos') ?></a>
                        </li>
                        <li>
                            <a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>" class="btn template-btn-1"><?php esc_html_e('checkout','narcos') ?></a>
                        </li>
                    </ul>
                    <?php else: ?>
                    <div class="subtotal"><?php esc_html_e('No products in the cart.','narcos');?></div>
                    <?php endif;?>
                </div>
            </div>
        <?php

        $fragments['div.shopping-cart-wrapper'] = ob_get_clean();

        return $fragments;
    }

    add_action('wp_ajax_update_cart_info', 'tt_update_cart_info');
    add_action('wp_ajax_nopriv_update_cart_info', 'tt_update_cart_info');

    function tt_update_cart_info() {
        $result = sprintf('<span class="cart-placeholder">%s (<span class="nr">%s</span>) <span class="sum">%s</span></span>', esc_html__('My cart','narcos'), WC()->cart->cart_contents_count, WC()->cart->cart_contents_total);
        wp_die(json_encode($result));
    }  

    if(!function_exists('tt_manage_action_woo')){
        function tt_manage_action_woo() {
            remove_action( 'woocommerce_single_product_summary','woocommerce_template_single_excerpt', 20 );
            add_action( 'woocommerce_single_product_summary','woocommerce_template_single_excerpt', 15 );
            remove_action( 'woocommerce_single_product_summary','woocommerce_template_single_price', 10 );
            add_action( 'woocommerce_single_product_summary','woocommerce_template_single_price', 20 );
            remove_action( 'woocommerce_single_product_summary','woocommerce_template_single_sharing', 50 );
            remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
            add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_tesla_thumbnail_carousel', 10 );
            remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
            add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_tesla_thumbnail_carousel', 10 );
            add_action( 'woocommerce_after_shop_loop_item', 'tt_woocommerce_quickview_btn', 20 );
            remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
            remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
            add_action( 'tt_woocommerce_cart_totals', 'woocommerce_cart_totals', 10 );
            remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );
        }
    }
    add_action('wp','tt_manage_action_woo');
    
    function tt_woocommerce_introbox($shop_id, $product_id = null) {
        $page_bg = get_post_meta( $shop_id, THEME_NAME . '_page_background', true );
        $page_slider = get_post_meta( $shop_id, THEME_NAME . '_rev_slider_alias', true );
        $product_slider = get_post_meta( $product_id, THEME_NAME . '_rev_slider_alias', true );
        $page_bg = $page_bg ? $page_bg['url'] : get_template_directory_uri().'/img/intro-box-shop.jpg';

        ob_start();
        if($product_slider) {
            echo do_shortcode( $product_slider );
        }
        else {
            if($page_slider)
                echo do_shortcode( $page_slider );
            else { ?>
                <!-- Intro Box -->
                <div class="intro-box" data-parallax-bg="<?php echo esc_url($page_bg);?>">
                    <div class="box-img-wrapper">
                        <div class="box-img">
                            <span></span>
                        </div>
                    </div>

                    <div class="intro-box-inner container">
                        <div class="box-content align-center">
                            <h2 class="page-title"><?php echo get_the_title($shop_id);?></h2>
                            <?php echo narcos_breadcrumbs();?>
                        </div>
                    </div>

                    <!-- Scroll Down Btn -->
                    <a href="#" class="scroll-btn">
                        <span class="icon"></span>
                    </a>

                    <!-- Rounded Border Bottom -->
                    <div class="rounded-border-bottom">
                        <span></span>
                    </div>
                </div>
            <?php }
        } 
        return ob_get_clean();
    }

    function woocommerce_tesla_thumbnail_carousel() {
        global $post, $product, $woocommerce;

        $product_images_id = array();
        $product_images_id[] = get_post_thumbnail_id();

        $attachment_ids = array_merge( $product_images_id, $product->get_gallery_attachment_ids());

        if ( count($attachment_ids) > 1 ) {
            ?>
            <!-- Navigation -->
            <div class="tt-slider product-cover-slider" data-speed="700" data-dots="false" data-arrows="true" data-fade="true" data-infinite="true">

                <ul class="slides-list clean-list"><?php

                    foreach ( $attachment_ids as $attachment_id ) {

                        $image_link = wp_get_attachment_url( $attachment_id );

                        if ( ! $image_link )
                            continue;

                        $image_class = '';
                        $image_title    = esc_attr( get_the_title( $attachment_id ) );
                        $image_caption  = esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );
                        $image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_catalog' ), 0, $attr = array(
                            'title' => $image_title,
                            'alt'   => $image_title
                        ) );

                        echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<li class="slide">%s</li>', $image), $attachment_id, $post->ID, $image_class );

                    } ?>

                </ul>

            </div>

            <?php
        }
    }

    function tt_woocommerce_quickview_btn() { ?>
        <a href="#" data-id="<?php the_ID() ?>" class="btn quick-view-btn">
            <i class="icon-search2"></i>
        </a>
    <?php
    }

    if(!function_exists('tt_return_products_on_page')){
        function tt_return_products_on_page() {

            if(_go('default_shop_nr_products')){
                $show_on_page = _go('default_shop_nr_products');
            } else {
                $show_on_page = '8';
            }

            return $show_on_page;
        }
    }
    add_filter( 'loop_shop_per_page', 'tt_return_products_on_page', 20 );
endif;

add_action('wp_ajax_nopriv_load_product_popup','tt_load_product_popup');
add_action('wp_ajax_load_product_popup','tt_load_product_popup');

function tt_load_product_popup(){
    if ( isset( $_POST['product_id'] ) ) :
        global $product;

        $product_id = $_POST['product_id'];
        $terms = get_the_terms( $product_id, 'product_cat' );
        $old_product = $product;
        $product = wc_get_product( $product_id );
        $product_title = get_the_title( $product_id );
        $product_url = get_the_permalink( $product_id );
        $thumbnail_id = get_post_thumbnail_id( $product_id );
        $gallery_ids = $product->get_gallery_attachment_ids();
        $product_images_id = array();

        if( $thumbnail_id )
            $product_images_id[] = $thumbnail_id;

        if( count( $gallery_ids ) > 0 )
            $product_images_id = array_merge( $product_images_id, $product->get_gallery_attachment_ids() );

        $rating_count = $product->get_rating_count();
        $review_count = $product->get_review_count();
        $average      = $product->get_average_rating();

        ob_start();?>
        <div class="row woocommerce">
            <div class="col-md-14">
                <div class="photo-slider-wrapper">
                    <div class="photo-slider clearfix">
                        <?php if(count($product_images_id) > 1): ?>
                        <!-- Navigation -->
                        <div class="tt-carousel-popup nav-carousel" data-items-desktop="4" data-items-small-desktop="3" data-items-tablet="2" data-items-phone="2" data-infinite="true" data-dots="false" data-arrows="false" data-items-to-slide="1" data-as-nav-for=".cover-photos-slider .slides-list" data-vertical="true">
                            <ul class="clean-list carousel-items">
                                <?php
                                    foreach ( $product_images_id as $attachment_id ) :

                                        $image_link = wp_get_attachment_url( $attachment_id );

                                        if ( ! $image_link )
                                            continue;

                                        $image_class = '';
                                        $image_title    = esc_attr( get_the_title( $attachment_id ) );
                                        $image_caption  = esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );
                                        $image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
                                            'title' => $image_caption,
                                            'alt'   => $image_title
                                        ) );

                                        echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<li class="carousel-item">%s</li>', $image), $attachment_id, $product_id, $image_class );

                                    endforeach
                                ?>
                            </ul>
                        </div>
                        <?php endif;?>
                        
                        <!-- Main Photos -->
                        <div class="tt-slider-popup cover-photos-slider" data-fade="true" data-speed="700" data-dots="false" data-arrows="false" data-infinite="true" data-as-nav-for=".nav-carousel .carousel-items">
                            <ul class="clean-list slides-list">
                                <?php
                                    foreach( $product_images_id as $product_image_id ):

                                        $image_link     = wp_get_attachment_url( $product_image_id );

                                        if ( ! $image_link )
                                            continue;

                                        $image_title    = esc_attr( get_the_title( $product_image_id ) );
                                        $image_caption  = esc_attr( get_post_field( 'post_excerpt', $product_image_id ) );

                                        printf( '<li class="slide"><img src="%s" title="%s" alt="%s" /></li>', $image_link, $image_caption, $image_title );

                                    endforeach
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-10">
                <!-- Product Description Box -->
                <div class="single-product-description-box">
                    <div class="box-header">
                        <h3 class="product-title"><a href="<?php echo esc_url($product_url);?>"><?php print $product_title ?></a></h3>
                    </div>

                    <div class="box-body">
                        <p><?php echo narcos_excerpt($product_id, 400); ?></p>

                        <?php if ( $rating_count > 0 ) : ?>

                        <div class="woocommerce-product-rating">
                            <div class="star-rating" title="<?php printf( esc_html__( 'Rated %s out of 5', 'narcos' ), $average ); ?>">
                                <span style="width:<?php echo ( ( $average / 5 ) * 100 ); ?>%">
                                    <strong itemprop="ratingValue" class="rating"><?php echo esc_html( $average ); ?></strong> <?php printf( esc_html__( 'out of %s5%s', 'narcos' ), '<span itemprop="bestRating">', '</span>' ); ?>
                                    <?php printf( _n( 'based on %s customer rating', 'based on %s customer ratings', $rating_count, 'narcos' ), '<span itemprop="ratingCount" class="rating">' . $rating_count . '</span>' ); ?>
                                </span>
                            </div>
                        </div>
                        
                        <?php endif; ?>

                        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            <?php echo $product->get_price_html(); ?>

                            <meta itemprop="price" content="<?php echo esc_attr( $product->get_price() ); ?>" />
                            <meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
                            <link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
                        </div>

                        <?php echo apply_filters( 'woocommerce_loop_add_to_cart_link',
                                sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
                                    esc_url( $product->add_to_cart_url() ),
                                    esc_attr( isset( $quantity ) ? $quantity : 1 ),
                                    esc_attr( $product->id ),
                                    esc_attr( $product->get_sku() ),
                                    esc_attr( 'button product_type_simple add_to_cart_button ajax_add_to_cart btn add-to-cart-btn ' ),
                                    esc_html( $product->add_to_cart_text() )
                                ),
                            $product ); ?>

                        <?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

                            <span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'narcos' ); ?> <span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'narcos' ); ?></span></span>

                        <?php endif; ?>

                        <?php echo $product->get_tags( '', '<div class="tagcloud">', '</div>' ); ?>

                        <div class="widget_categories">
                            <ul>
                                <?php
                                if(!is_wp_error( $terms ) || !empty($terms)) {
                                    foreach ( $terms as $term ) {
                                        $link = get_term_link( $term, 'product_cat' );
                                        if ( ! is_wp_error( $link ) ) {
                                          printf('<li><a href="%s">%s</a></li>', $link, $term->name);
                                        }
                                    }
                                }
                                ?>
                            </ul>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        
        <?php $output = ob_get_contents();
        ob_end_clean();
        wp_reset_query();
        $product = $old_product;
        die($output);
    endif;
}