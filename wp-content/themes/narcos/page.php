<?php get_header(); ?>	

<?php
/**
 * Page
 */
$page_id = narcos_get_page_id();
$sidebar = get_post_meta( $page_id, THEME_NAME . '_sidebar_position', true );
$rev_slider = get_post_meta( $page_id, THEME_NAME . '_rev_slider_alias', true );
$sidebar = $sidebar ? $sidebar : 'right';
?>

<?php if(empty($rev_slider))
	get_template_part('templates/intro','box');
else if($rev_slider !== 'false'):?>
	<!-- Intro Box -->
	<div class="intro-box">
		<?php echo do_shortcode($rev_slider);?>

		<!-- Scroll Down Btn -->
		<a href="#" class="scroll-btn">
			<span class="icon"></span>
		</a>

		<!-- Rounded Border Bottom -->
		<div class="rounded-border-bottom">
			<span></span>
		</div>

		<span class="after-rounded-bottom"></span>
	</div>
<?php endif;?>

<?php while ( have_posts() ) : the_post();?>
	<?php if(!has_shortcode(get_the_content(),'vc_row') || ($sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
	<section class="section section-blog no-margin">
		<div class="row">
	<?php endif ?>	
			<?php if($sidebar == "left"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>

			<?php if($sidebar!= 'full_width'): ?>
			<div class="<?php print $sidebar == "full_width" ? 'col-lg-12 col-lg-offset-6 col-md-18 col-md-offset-3' : 'col-lg-12 col-lg-offset-2 col-md-15';?>">
				<div class="single-post-content">
			<?php endif;?>

					<?php the_content();?>

			<?php if($sidebar!= 'full_width'): ?>
				</div>
			</div>
			<?php endif;?>

			<?php if($sidebar == "right"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>
	<?php if(!has_shortcode(get_the_content(),'vc_row') || ($sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
		</div>
	</section>
	<?php endif;?>

	<?php comments_template();?>

<?php endwhile; ?>
<?php get_footer();?>