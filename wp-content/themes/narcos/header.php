<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
    

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

     <!-- Pingbacks -->
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<?php wp_head(); ?>

<!-- Codigo Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80054565-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Codigo Webmastertools -->
<meta name="google-site-verification" content="-G9E3AiPsAtikbGEbk2iYBYW0z50_ZAQyazvMonG5Zs" />
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '650651155145393'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=650651155145393&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->



</head>
<?php 
	$page_id = narcos_get_page_id();
	$header = get_post_meta($page_id, THEME_NAME . '_header_type', true ) ? get_post_meta($page_id, THEME_NAME . '_header_type', true ) : (_go('header_sticky') ? _go('header_sticky') : 'no-sticky');
	$p_logo = get_post_meta($page_id, THEME_NAME.'_logo_pos', true);
	$p_menu_s = get_post_meta($page_id, THEME_NAME.'_menu_style', true);

	$logo_position = $p_logo ? $p_logo : (_go('logo_position') ? _go('logo_position') : 'logo-left');
	$menu_style = $p_menu_s ? $p_menu_s : (_go('menu_style') ? _go('menu_style') : 'expanded');
;?>
<body <?php body_class($header);?>>
	<div id="page">
		<!-- Search Form Wrapper -->
		<div class="search-form-wrapper">
			<form class="search-form" method="get" role="search" action="<?php echo home_url('/') ?>">
				<input type="text" class="form-input check-value" name="s" id="s" value="" placeholder="<?php esc_html_e('Start typing...','narcos');?>" />
				<button class="form-submit">
					<i class="icon-search"></i>
				</button>
			</form>

			<span class="close-search-form-wrapper">
				<i class="icon-cross"></i>
			</span>
		</div>

		<!-- Video Popup -->
		<div class="popup-wrapper video-popup">
			<!-- Close Video Popup -->
			<span class="close-video-popup-toggle"></span>
			
			<div class="popup-inner-content">
				<div class="media-wrapper">
					<div class="responsive-media ratio16by9"></div>
				</div>
			</div>
		</div>

		<?php if($menu_style == 'boxed'):?>
		<!-- Stack Navigation -->
		<div class="stack-nav-wrapper">
			<span class="close-stack-nav"></span>

			<nav class="stack-nav">
				<ul class="clean-list">
					<?php wp_nav_menu( array( 
                        'title_li'=>'',
                        'theme_location' => 'secondary_menu',
                        'container' => false,
                        'items_wrap' => '%3$s',
                        'fallback_cb' => 'wp_list_pages'
                        ));
                    ?>
				</ul>
			</nav>

			<a href="<?php echo home_url('/'); ?>" style="<?php _estyle_changer('logo_text') ?>" class="identity-wrapper">
                <?php if(_go('logo_image')): ?>
                    <img src="<?php _eo('logo_image') ?>" alt="<?php echo THEME_PRETTY_NAME ?>">
                <?php elseif(_go('logo_text')): ?>
                    <?php _eo('logo_text') ?>
                <?php else: ?>
                     <img src="<?php echo get_template_directory_uri().'/img/brand.png';?>" alt="<?php echo THEME_PRETTY_NAME ?>">
                <?php endif; ?>
            </a>
		</div>
		<?php endif;?>

		<?php if(tesla_has_woocommerce()):?>
		<!-- Quick View Popup -->
		<div class="popup-wrapper">
			<div class="popup-inner-content">
				<div class="quick-view-box">
					<!-- Close Popup -->
					<span class="close-popup-btn">
						<i class="icon-cross"></i>
					</span>

					<div class="target"></div>
				</div>
			</div>
		</div>
		<?php endif;?>

		<!-- Page Wrapper -->
		<div class="page-wrapper">

			<!-- Header -->
			<header class="main-header <?php echo esc_attr($logo_position.' '.$menu_style);?>">
				<!-- Site Identity -->
				<?php if($logo_position == 'logo-center' && $menu_style == 'expanded'):?>
				<a href="<?php echo home_url('/'); ?>" style="<?php _estyle_changer('logo_text') ?>" class="mobile-brand">
                    <?php if(_go('logo_image')): ?>
                        <img src="<?php _eo('logo_image') ?>" alt="<?php echo THEME_PRETTY_NAME ?>">
                    <?php elseif(_go('logo_text')): ?>
                        <?php _eo('logo_text') ?>
                    <?php else: ?>
                         <img src="<?php echo get_template_directory_uri().'/img/brand.png';?>" alt="<?php echo THEME_PRETTY_NAME ?>">
                    <?php endif; ?>
                </a>
            	<?php endif;?>

				<a href="<?php echo home_url('/'); ?>" style="<?php _estyle_changer('logo_text') ?>" class="brand-holder">
                    <?php if(_go('logo_image')): ?>
                        <img src="<?php _eo('logo_image') ?>" alt="<?php echo THEME_PRETTY_NAME ?>">
                    <?php elseif(_go('logo_text')): ?>
                        <?php _eo('logo_text') ?>
                    <?php else: ?>
                         <img src="<?php echo get_template_directory_uri().'/img/brand.png';?>" alt="<?php echo THEME_PRETTY_NAME ?>">
                    <?php endif; ?>
                </a>

				<nav>
					<?php if($menu_style == 'expanded'):?>
					<!-- Menu Items -->
					<ul>
						<?php wp_nav_menu( array( 
	                        'title_li'=>'',
	                        'theme_location' => 'main_menu',
	                        'container' => false,
	                        'items_wrap' => '%3$s',
	                        'fallback_cb' => 'wp_list_pages'
	                        ));
	                    ?>
					</ul>
					<?php endif;?>

					<!-- Nav Toggle -->
					<span class="<?php print $menu_style == 'expanded' ? 'mobile-navigation-toggle' : 'stack-menu-toggle';?>">
						<span class="icon">
							<i class="line"></i>
							<i class="line"></i>
							<i class="line"></i>
						</span>

						<span class="menu-toggle-title"><?php esc_html_e('Menu','narcos');?></span>
					</span>

					<!-- Search Toggle -->
					<span class="search-toggle">
						<i class="icon-search"></i>
					</span>

					<?php if(tesla_has_woocommerce()): ?>
					<div class="shopping-cart-wrapper">
						<span class="cart-toggle no-select">
							<i class="icon-shoppingbag"></i>
							<span class="cart-items"><?php echo WC()->cart->cart_contents_count;?></span>
						</span>
	
						<div class="cart-items-wrapper">
							<?php if(WC()->cart->cart_contents_count): ?>
							<ul class="clean-list cart-items">
								<?php foreach(WC()->cart->get_cart() as $cart_item_key => $cart_item):
                                    $_product = $cart_item['data'];
                                    if ((!apply_filters('woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key) || !$_product->exists() || $cart_item['quantity'] == 0)) { continue; };
                                    $product_name  = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
                                    $thumbnail     = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image('tt_shop_mini_cart'), $cart_item, $cart_item_key );
                                    $product_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                                    $product_link  = get_permalink($cart_item['product_id']);
                                    ?>
                                    <li class="cart-item">
										<div class="image">
											<?php
	                                            if ( ! $_product->is_visible() ) {
	                                                echo "$thumbnail";
	                                            } else {
	                                                printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
	                                            }
	                                            ?>
										</div>

										<?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove-product-from-cart" title="%s"><i class="icon-cross"></i></a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), esc_html__( 'Remove this item','narcos' ) ), $cart_item_key ); ?>

										<h6 class="product-title">
											 <a href="<?php echo esc_url($product_link); ?>"><?php print $product_name ?></a>
										</h6>

										<?php printf( '<span class="meta">%s<sub>%s</sub>%s</span>', $cart_item['quantity'], 'x', $product_price);?>
										
									</li>
                                <?php endforeach ?>
							</ul>

							<div class="subtotal"><?php echo esc_html__('Subtotal: ','narcos') . WC()->cart->get_cart_subtotal(); ?></div>

							<ul class="clean-list cart-action clearfix">
								<li>
									<a href="<?php echo get_permalink( wc_get_page_id( 'cart' ) ); ?>" class="btn template-btn-1"><?php esc_html_e('view cart','narcos') ?></a>
								</li>
								<li>
									<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>" class="btn template-btn-1"><?php esc_html_e('checkout','narcos') ?></a>
								</li>
							</ul>
							<?php else: ?>
							<div class="subtotal"><?php esc_html_e('No products in the cart.','narcos');?></div>
							<?php endif;?>
						</div>
					</div>
					<?php endif;?>
				</nav>
			</header>

			<!-- Main Content -->
			<div class="content-wrapper">